// MODULE SECTION
module.exports = function(RED) {
	var i2cBus = require("i2c-bus");
	function relaymoduleNode(n) {
		RED.nodes.createNode(this,n);
        this.addr = parseInt(n.addr, 16);
        this.bus = parseInt(n.bus);
		this.ids = new Array(8);
		this.lastState = 0x0000;
		
		this.i2c1 = i2cBus.openSync(this.bus);
		this.i2c1.writeByteSync(this.addr, 0x00, 0x00); // IODIR

		this.setBit = function(channel, id){
			
			for (var i=0; i < 8; i++){
				if (this.ids[i] == id) {this.ids[i] = null;}
			}
			this.ids[channel] = id;
            this.i2c1.writeByteSync(this.addr, 0x0a, this.lastState & 0xFF); // OLAT
			this.i2c1.writeByteSync(this.addr, 0x00, 0); // IODIR
		}
		
		this.setOutput = function(channel, newState){
			if (newState){
				this.lastState = this.lastState | 1 << channel;
			} else {
				this.lastState = this.lastState & ~ (1 << channel);
			}
            this.i2c1.writeByteSync(this.addr, 0x0a, this.lastState & 0xFF); // OLAT
			this.i2c1.writeByteSync(this.addr, 0x00, 0x00); // IODIR
		}
			
		this.on('close', function() {
			this.i2c1.closeSync();
		});
	}
	RED.nodes.registerType("relaymodule",relaymoduleNode);

//OUTPUT SECTION
	function relaychannelNode(config) {
		RED.nodes.createNode(this,config);
		var node = this;
		this.addr = config.addr;
		this.channel = config.channel;
		
		this.myModule = RED.nodes.getNode(config.module);
		this.myModule.setBit(this.channel, this.id);
		
		this.on('input', function(msg) {
			this.myModule.setOutput(this.channel, msg.payload)
			if (msg.payload) {
				this.status({fill:"green",shape:"dot",text:"on"});
			} else {
				this.status({fill:"red",shape:"ring",text:"off"});
			}
		});

	}
	RED.nodes.registerType("relaychannel",relaychannelNode);
}
