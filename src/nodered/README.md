Node-Red node for the I2C relay module

Requires i2c-bus

Install as: 
npm install -g node-red-contrib-relayboard-i2c

or on the Node-Red UI:
select Manage Palette from the top-right menu,
select the Install tab,
press the upload button,
select the package file "node-red-contrib-relayboard-i2c.tgz",
press the Upload button

An example flow is available for testing.
On the Node-Red UI:
Press Ctrl+I, select file to import, select example-flows.json

This code is based on Mike Wilson's mcp23017chip code available at https://github.com/willhey/mcp23017chip
